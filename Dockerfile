FROM openjdk:17
EXPOSE 9191
COPY target/Api-Endpoint-1.0.jar /Api-Endpoint-1.0.jar
ENTRYPOINT ["java","-jar","/Api-Endpoint-1.0.jar"]